package main

import "fmt"
import "time"
import "github.com/nats-io/go-nats"

func main() {
  nc, _ := nats.Connect("nats://localhost:4222")
	/*if err =! nil {
		fmt.Printf(fmt.Sprintf("%v", err))
	}*/
	for {
		timer := 2000 * time.Millisecond
		t := time.Now()
		o := fmt.Sprintf("%d.%d.%d.%d.%d.%d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())
		fmt.Printf(fmt.Sprintf("Publish: %s\n", o))
		nc.Publish("topic1", []byte(fmt.Sprintf("%s", o)))
		time.Sleep(timer)
	}
}
