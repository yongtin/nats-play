package main

import "fmt"
import "github.com/nats-io/go-nats"

func main() {
  nc, _ := nats.Connect("nats://localhost:4222")
	/*if err =! nil {
		fmt.Printf(fmt.Sprintf("%v", err))
	}*/
	// Channel Subscriber
	ch := make(chan *nats.Msg, 64)
	sub, _ := nc.ChanSubscribe("topic1", ch)
  for { 
	  msg := <-ch
	  fmt.Printf(fmt.Sprintf("Received: %s\n", string(msg.Data)))
  }

	// Unsubscribe
	sub.Unsubscribe()

}
